package com.example.nasapictureapp.datareader

import com.example.nasapictureapp.datasource.ResponseHandler
import com.example.nasapictureapp.model.PictureDetails
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.io.FileNotFoundException
import java.io.IOException

@RunWith(JUnit4::class)
class ResponseHandlerTest {
    lateinit var responseHandler: ResponseHandler

    @Before
    fun setUp() {
        responseHandler = ResponseHandler()
    }

    @Test
    fun `when exception is FileNotFoundException then return file not found`() {
        val exception = FileNotFoundException()
        val result = responseHandler.handleException<PictureDetails>(exception)
        Assert.assertEquals("File not found", result.message)
    }

    @Test
    fun `when exception is IOException then return could not able to read data from file`() {
        val exception = IOException()
        val result = responseHandler.handleException<PictureDetails>(exception)
        Assert.assertEquals("Could not able to read data from file", result.message)
    }

    @Test
    fun `when exception is NullPointerException then return something went wrong could not able to fetch data`() {
        val exception = NullPointerException()
        val result = responseHandler.handleException<PictureDetails>(exception)
        Assert.assertEquals("Something went wrong. Could not able to fetch data", result.message)
    }
}