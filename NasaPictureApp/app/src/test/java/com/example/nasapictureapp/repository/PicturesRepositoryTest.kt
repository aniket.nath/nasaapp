package com.example.nasapictureapp.repository

import com.example.nasapictureapp.datasource.DataReader
import com.example.nasapictureapp.datasource.Resource
import com.example.nasapictureapp.datasource.ResponseHandler
import com.example.nasapictureapp.model.PictureDetails
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PicturesRepositoryTest {
    private val responseHandler = ResponseHandler()
    private lateinit var dataReader: DataReader
    private lateinit var repository: PicturesRepository
    private val pictureDetails = PictureDetails(
        "ESA/HubbleNASA",
        "2019-12-01",
        "Why does this galaxy have a ring of bright blue stars?",
        "https://apod.nasa.gov/apod/image/1912/M94_Hubble_1002.jpg",
        "image",
        "v1",
        "Starburst Galaxy M94 from Hubble",
        "https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")
    private val pictureDetailsList: MutableList<PictureDetails> = ArrayList()
    private val pictureDetailsResponse = Resource.success(pictureDetailsList)

    @Before
    fun setUp() {
        dataReader = mock()
        pictureDetailsList.add(pictureDetails)
        runBlocking {
            whenever(dataReader.getPictureDetailsList()).thenReturn(pictureDetailsList)
        }
        repository = PicturesRepository(
            dataReader,
            responseHandler
        )
    }

    @Test
    fun `test get picture details from picture details repository`() =
        runBlocking {
            assertEquals(pictureDetailsResponse, repository.getPictureDetails())
        }
}