package com.example.nasapictureapp.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nasapictureapp.datasource.Resource
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.repository.PicturesRepository
import com.example.nasapictureapp.ui.viewmodels.PicturesViewModel
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.timeout
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class PicturesViewModelTest {
    private lateinit var viewModel: PicturesViewModel
    private lateinit var repository: PicturesRepository
    private lateinit var observer: Observer<Resource<List<PictureDetails>>>
    private val pictureDetails = PictureDetails(
        "ESA/HubbleNASA",
        "2019-12-01",
        "Why does this galaxy have a ring of bright blue stars?",
        "https://apod.nasa.gov/apod/image/1912/M94_Hubble_1002.jpg",
        "image",
        "v1",
        "Starburst Galaxy M94 from Hubble",
        "https://apod.nasa.gov/apod/image/1912/M94_Hubble_960.jpg")
    private val pictureDetailsList: MutableList<PictureDetails> = ArrayList()
    private val successResource = Resource.success(pictureDetailsList)

    @ObsoleteCoroutinesApi
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @ExperimentalCoroutinesApi
    @ObsoleteCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        repository = mock()
        pictureDetailsList.add(pictureDetails)
        runBlocking {
            whenever(repository.getPictureDetails()).thenReturn(successResource)
        }
        viewModel = PicturesViewModel(repository)
        observer = mock()
    }

    @ObsoleteCoroutinesApi
    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `when getPictureDetails gets called observer is updated with success`() = runBlocking {
        viewModel.pictureDetails.observeForever(observer)
        delay(10)
        verify(repository).getPictureDetails()
        verify(observer, timeout(50)).onChanged(Resource.loading(null))
        verify(observer, timeout(50)).onChanged(successResource)
    }
}