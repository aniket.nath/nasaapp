package com.example.nasapictureapp.datasource

import java.io.FileNotFoundException
import java.io.IOException
import java.lang.Exception

open class ResponseHandler {
    fun <T : Any> handleSuccess(data: T): Resource<T> {
        return Resource.success(data)
    }

    fun <T : Any> handleException(e: Exception): Resource<T> {
        return when (e) {
            is FileNotFoundException -> Resource.error(getErrorMessage(101), null)
            is IOException -> Resource.error(getErrorMessage(102), null)
            else -> Resource.error(getErrorMessage(Int.MAX_VALUE), null)
        }
    }

    private fun getErrorMessage(code: Int): String {
        return when (code) {
            101 -> "File not found"
            102 -> "Could not able to read data from file"
            else -> "Something went wrong. Could not able to fetch data"
        }
    }
}
