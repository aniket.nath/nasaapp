package com.example.nasapictureapp.ui.screens

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.example.nasapictureapp.R
import com.example.nasapictureapp.databinding.PictureDescriptionFragmentBinding
import com.example.nasapictureapp.model.PictureDetails
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class PictureDescriptionFragment : BottomSheetDialogFragment(), View.OnClickListener {

    private lateinit var binding: PictureDescriptionFragmentBinding
    private var picturesDetails: PictureDetails?= null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.setOnShowListener { dialogInterface ->
            val bottomSheetDialog = dialogInterface as BottomSheetDialog
            setupFullHeight(bottomSheetDialog)
        }
        return dialog
    }

    private fun setupFullHeight(bottomSheetDialog: BottomSheetDialog) {
        val bottomSheet = bottomSheetDialog
            .findViewById<FrameLayout>(R.id.design_bottom_sheet)
        val behavior: BottomSheetBehavior<*>?
        if (bottomSheet != null) {
            behavior = BottomSheetBehavior.from(bottomSheet)
            behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PictureDescriptionFragmentBinding.inflate(inflater, container, false)
        initArguments()
        setupScreen()
        return binding.root
    }

    private fun initArguments() {
        picturesDetails = arguments?.getParcelable("pictureDetails")
    }

    private fun setupScreen() {
        binding.apply {
            cancelDialog.setOnClickListener(this@PictureDescriptionFragment)
            pictureDetails = picturesDetails
        }
    }

    override fun onClick(v: View?) {
        dismiss()
    }
}