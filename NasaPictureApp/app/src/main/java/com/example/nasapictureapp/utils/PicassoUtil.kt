package com.example.nasapictureapp.utils

import android.widget.ImageView
import com.example.nasapictureapp.R
import com.squareup.picasso.Picasso

object PicassoUtil {
    fun setImage(url: String?, imageView: ImageView){
        Picasso.get().load(url).placeholder(R.drawable.placeholder).fit()
                .into(imageView)

    }
}