package com.example.nasapictureapp.di

import com.example.nasapictureapp.datasource.DataReader
import com.example.nasapictureapp.datasource.ResponseHandler
import com.example.nasapictureapp.listeners.OnListItemClick
import com.example.nasapictureapp.repository.PicturesRepository
import com.example.nasapictureapp.ui.adapter.ImagesAdapter
import com.example.nasapictureapp.ui.adapter.PageAdapter
import com.example.nasapictureapp.ui.viewmodels.PicturesViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val appModule = module {
    factory { DataReader(androidApplication()) }
    factory { ResponseHandler() }
}

val repositoryModule = module {
    factory { PicturesRepository(get(), get()) }
}

val adaptersModule = module {
    factory { (listener: OnListItemClick) -> ImagesAdapter(listener) }
    factory { (listener: OnListItemClick) -> PageAdapter(androidApplication(), listener) }
}

val viewModelModule = module {
    factory { PicturesViewModel(get()) }
}