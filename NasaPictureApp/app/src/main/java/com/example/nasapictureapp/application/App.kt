package com.example.nasapictureapp.application

import android.app.Application
import com.example.nasapictureapp.di.adaptersModule
import com.example.nasapictureapp.di.appModule
import com.example.nasapictureapp.di.repositoryModule
import com.example.nasapictureapp.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule, repositoryModule, adaptersModule, viewModelModule)
        }
    }
}