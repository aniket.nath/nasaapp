package com.example.nasapictureapp.datasource

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}