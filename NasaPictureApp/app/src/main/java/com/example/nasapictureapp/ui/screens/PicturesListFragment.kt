package com.example.nasapictureapp.ui.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.nasapictureapp.R
import com.example.nasapictureapp.databinding.PicturesListFragmentBinding
import com.example.nasapictureapp.datasource.Status
import com.example.nasapictureapp.listeners.OnListItemClick
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.ui.MainActivity
import com.example.nasapictureapp.ui.adapter.ImagesAdapter
import com.example.nasapictureapp.ui.viewmodels.PicturesViewModel
import com.example.nasapictureapp.utils.PictureDetailsCache
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class PicturesListFragment : Fragment(), OnListItemClick {

    private val viewModel: PicturesViewModel by viewModel()
    private lateinit var binding: PicturesListFragmentBinding
    private val adapter: ImagesAdapter by inject { parametersOf(this@PicturesListFragment) }
    private var picturesDetailsList: List<PictureDetails>? = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PicturesListFragmentBinding.inflate(inflater, container, false)
        setupActionBar()
        initRecyclerView()
        fetchPictureDetails()
        return binding.root
    }

    private fun setupActionBar() {
        val actionBar = (activity as MainActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(false)
        actionBar?.setHomeButtonEnabled(false)
        actionBar?.title = getString(R.string.pictures)
    }

    private fun initRecyclerView() {
        binding.apply {
            nasaPicturesList.layoutManager = GridLayoutManager(requireActivity(), 2)
            nasaPicturesList.adapter = adapter
        }
    }

    private fun fetchPictureDetails() {
        viewModel.pictureDetails.observe(viewLifecycleOwner, {
            when(it.status){
                Status.LOADING -> showLoading()
                Status.SUCCESS -> handleSuccess(it.data)
                Status.ERROR -> showError(it.message)
            }
        })
    }

    private fun showLoading() {
        binding.message.text = getString(R.string.loading)
    }

    private fun handleSuccess(data: List<PictureDetails>?) {
        picturesDetailsList = data
        binding.message.visibility = View.GONE
        updateAdapter()
    }

    private fun updateAdapter() {
        adapter.addItems(picturesDetailsList!!)
    }

    private fun showError(message: String?) {
        binding.message.text = message
    }

    override fun onListItemClick(position: Int?) {
        val bundle = bundleOf("position" to position)
        PictureDetailsCache.setPictureDetails(picturesDetailsList!!)
        binding.root.findNavController().navigate(R.id.next_action, bundle)
    }
}