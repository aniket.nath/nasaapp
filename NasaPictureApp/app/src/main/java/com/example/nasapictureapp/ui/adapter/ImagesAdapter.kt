package com.example.nasapictureapp.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nasapictureapp.databinding.ItemPictureListBinding
import com.example.nasapictureapp.listeners.OnListItemClick
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.utils.PicassoUtil

class ImagesAdapter(private val listener: OnListItemClick) :
        RecyclerView.Adapter<ImagesAdapter.ViewHolder>() {

    private var items: List<PictureDetails> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemsBinding = ItemPictureListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent, false)
        return ViewHolder(itemsBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setContent(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(items: List<PictureDetails>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val itemBinding: ItemPictureListBinding) :
            RecyclerView.ViewHolder(itemBinding.root),
            View.OnClickListener {
        init {
            itemBinding.root.setOnClickListener(this)
        }
        override fun onClick(v: View?) {
            listener.onListItemClick(adapterPosition)
        }

        fun setContent(pictureDetails: PictureDetails) {
            PicassoUtil.setImage(pictureDetails.url, itemBinding.nasaPicture)
        }
    }
}