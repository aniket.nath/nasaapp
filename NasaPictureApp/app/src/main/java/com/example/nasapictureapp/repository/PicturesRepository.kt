package com.example.nasapictureapp.repository

import com.example.nasapictureapp.datasource.DataReader
import com.example.nasapictureapp.datasource.Resource
import com.example.nasapictureapp.datasource.ResponseHandler
import com.example.nasapictureapp.model.PictureDetails

class PicturesRepository(private val dataReader: DataReader,
                         private val responseHandler: ResponseHandler) {

    suspend fun getPictureDetails(): Resource<List<PictureDetails>> {
        return try {
            val response = dataReader.getPictureDetailsList()
            return responseHandler.handleSuccess(response)
        } catch (e: Exception) {
            responseHandler.handleException(e)
        }
    }
}