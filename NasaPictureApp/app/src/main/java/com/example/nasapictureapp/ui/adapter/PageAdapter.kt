package com.example.nasapictureapp.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.example.nasapictureapp.databinding.ItemFullImageBinding
import com.example.nasapictureapp.listeners.OnListItemClick
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.utils.PicassoUtil


class PageAdapter(private val context: Context, private val listener: OnListItemClick) :
    PagerAdapter() {
    private var items: List<PictureDetails> = ArrayList()

    override fun getCount(): Int {
        return items.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater: LayoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val binding = ItemFullImageBinding.inflate(inflater, container, false)
        binding.apply {
            PicassoUtil.setImage(items[position].url, imageView)
            description.setOnClickListener(OnDescriptionClickListener(position))
            container.addView(root)
            return root
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    fun addItems(items: List<PictureDetails>) {
        this.items = items
        notifyDataSetChanged()
    }

    inner class OnDescriptionClickListener(private val position: Int) : View.OnClickListener {
        override fun onClick(v: View?) {
            listener.onListItemClick(position)
        }
    }
}
