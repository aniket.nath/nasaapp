package com.example.nasapictureapp.utils

import com.example.nasapictureapp.model.PictureDetails

object PictureDetailsCache {
    private val pictureDetailsList: MutableList<PictureDetails> = ArrayList()

    fun setPictureDetails(pictureDetails: List<PictureDetails>) {
        if (pictureDetailsList.isEmpty()) {
            pictureDetailsList.addAll(pictureDetails)
        }
    }

    fun getPictureDetails(): List<PictureDetails> {
        return pictureDetailsList
    }

    fun clearCache() {
        pictureDetailsList.clear()
    }
}