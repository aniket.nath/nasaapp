package com.example.nasapictureapp.ui.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.viewpager.widget.ViewPager
import com.example.nasapictureapp.R
import com.example.nasapictureapp.databinding.PicturesShowFragmentBinding
import com.example.nasapictureapp.listeners.OnListItemClick
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.ui.MainActivity
import com.example.nasapictureapp.ui.adapter.PageAdapter
import com.example.nasapictureapp.utils.PictureDetailsCache
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class PicturesShowFragment : Fragment(), OnListItemClick, ViewPager.OnPageChangeListener {

    private var actionBar: ActionBar?= null
    private lateinit var binding: PicturesShowFragmentBinding
    private val adapter: PageAdapter by inject {
        parametersOf(
            this@PicturesShowFragment
        )
    }
    private var picturesDetailsList: List<PictureDetails>? = ArrayList()
    private var position: Int?= 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = PicturesShowFragmentBinding.inflate(inflater, container, false)
        initArguments()
        setupActionBar()
        initViewPager()
        return binding.root
    }

    private fun initArguments() {
        position = arguments?.getInt("position")
        picturesDetailsList = PictureDetailsCache.getPictureDetails()
    }

    private fun setupActionBar() {
        actionBar = (activity as MainActivity).supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
        actionBar?.title = picturesDetailsList?.get(position!!)?.title
    }

    private fun initViewPager() {
        binding.viewPager.adapter = adapter
        adapter.addItems(picturesDetailsList!!)
        binding.viewPager.currentItem = position!!
        binding.viewPager.addOnPageChangeListener(this)
    }

    override fun onListItemClick(position: Int?) {
        val bundle = bundleOf("pictureDetails" to position?.let { picturesDetailsList?.get(it) })
        binding.root.findNavController().navigate(R.id.dialog_action, bundle)
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        actionBar?.title = picturesDetailsList?.get(position)?.title
    }

    override fun onPageSelected(position: Int) {

    }

    override fun onPageScrollStateChanged(state: Int) {

    }
}