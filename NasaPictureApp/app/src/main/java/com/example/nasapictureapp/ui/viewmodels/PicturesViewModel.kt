package com.example.nasapictureapp.ui.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.nasapictureapp.datasource.Resource
import com.example.nasapictureapp.repository.PicturesRepository
import kotlinx.coroutines.Dispatchers

class PicturesViewModel(private val repository: PicturesRepository) : ViewModel() {
    val pictureDetails = liveData(Dispatchers.IO) {
        emit(Resource.loading(null))
        emit(repository.getPictureDetails())
    }
}