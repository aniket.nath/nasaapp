package com.example.nasapictureapp.datasource

import android.content.Context
import com.google.gson.stream.JsonReader;
import com.example.nasapictureapp.model.PictureDetails
import com.example.nasapictureapp.singletonholder.SingletonHolder
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.lang.reflect.Type

class DataReader(private val context: Context) {
    companion object : SingletonHolder<DataReader, Context>(::DataReader)

    private val PICTURE_DETAILS_FORMAT: Type = object : TypeToken<List<PictureDetails>>() {}.type

    fun getPictureDetailsList(): List<PictureDetails> {
        val gson = Gson()
        val pictureDetails: MutableList<PictureDetails>
        try {
            val pictureDetailsFileName = "data.json"
            val stream: InputStream = context.assets
                    .open(pictureDetailsFileName)
            val reader = JsonReader(InputStreamReader(stream))
            pictureDetails = gson.fromJson(reader, PICTURE_DETAILS_FORMAT)
        } catch (e: FileNotFoundException) {
            throw e
        } catch (a: IOException) {
            throw a
        }
        return pictureDetails
    }
}