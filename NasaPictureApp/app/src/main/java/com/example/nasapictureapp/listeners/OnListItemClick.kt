package com.example.nasapictureapp.listeners

interface OnListItemClick {
    fun onListItemClick(position: Int?)
}