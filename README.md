# NasaApp


- Screens :
There are two screens in the project

1. On the first screen ypu can see list of Nasa pictures in tabular form and every row contains two images
2. If you click on any image it will navigate on the second screen
3. On the second screen you can see the full image and you can swap next or privious images
4. On the top of the image you can see the title of the image on the action bar
5. At the bottom of the image you can find a text as "Read Description>>>>"
6. By clicking on the page the full description of the image will be visible on a bottom sheet.


- Code structure :
Code is written in MVVM(Model-View-ViewModel) format

- Libraries Used :
1. Gson
2. Picasso
3. Navigation
3. Android Lifecycle Libraries
4. Koin DI 
5. Coroutines
6. Material library
7. Mocito
8. JUnit


- Language Used:
Kotlin


- Test cases written:
1. Test case for repository
2. Test case for ViewModel
3. Test case for Response Handler

